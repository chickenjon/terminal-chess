board = []


class Piece:
    def __init__(self, color, location):
        self.color = color
        self.location = location
        self.not_moved = True

    def is_white(self):
        return self.color == 'white'

    def is_black(self):
        return self.color == 'black'


class Pawn(Piece):
    def __init__(self, color, location):
        super().__init__(color, location)

    def legal_squares(self):
        squares = []

        if self.is_white():
            if (
                self.not_moved
                and board[self.location[0]][self.location[1] + 1] == 0
                and board[self.location[0]][self.location[1] + 2] == 0
            ):
                squares.extend(
                    [[self.location[0], self.location[1] + 1],
                    [self.location[0], self.location[1] + 2]]
                    )

            elif board[self.location[0]][self.location[1] + 1] == 0:
                squares.append([self.location[0], self.location[1] + 1])

            try:
                if board[self.location[0] - 1][self.location[1] + 1].is_black():
                    squares.append([self.location[0] - 1, self.location[1] + 1])
            except:
                pass

            try:
                if board[self.location[0] + 1][self.location[1] + 1].is_black():
                    squares.append([self.location[0] + 1, self.location[1] + 1])
            except:
                pass

        if self.is_black():
            if (
                self.not_moved
                and board[self.location[0]][self.location[1] - 1] == 0
                and board[self.location[0]][self.location[1] - 2] == 0
            ):
                squares.extend(
                    [[self.location[0], self.location[1] - 1],
                    [self.location[0], self.location[1] - 2]]
                    )

            elif board[self.location[0]][self.location[1] - 1] == 0:
                squares.append([self.location[0], self.location[1] - 1])

            try:
                if board[self.location[0] - 1][self.location[1] - 1].is_white():
                    squares.append([self.location[0] - 1, self.location[1] - 1])
            except:
                pass

            try:
                if board[self.location[0] + 1][self.location[1] - 1].is_white():
                    squares.append([self.location[0] + 1, self.location[1] - 1])
            except:
                pass

        return squares


class Knight(Piece):
    def __init__(self, color, location):
        super().__init__(color, location)

    def legal_squares(self):
        squares = []

        for x in [-2, -1, 1, 2]:
            for y in [-2, -1, 1, 2]:
                if abs(x) != abs(y):
                    try:
                        if (
                            board[self.location[0] + x][self.location[1] + y] == 0
                            or board[self.location[0] + x][self.location[1] + y].is_white() == self.is_black()
                        ):
                            squares.append([self.location[0] + x, self.location[1] + y])
                    except:
                        pass
        
        return squares


class Rook(Piece):
    def __init__(self, color, location):
        super().__init__(color, location)

    def legal_squares(self):
        squares = []

        i = 1
        while (
            self.location[0] + i < 8
            and board[self.location[0] + i][self.location[1]] == 0
        ):
            squares.append([self.location[0] + i, self.location[1]])
            i += 1
        try:
            if board[self.location[0] + i][self.location[1]].is_white() == self.is_black():
                squares.append([self.location[0] + i, self.location[1]])
        except:
            pass

        i = 1
        while (
            self.location[0] - i >= 0
            and board[self.location[0] - i][self.location[1]] == 0
        ):
            squares.append([self.location[0] - i, self.location[1]])
            i += 1
        try:
            if board[self.location[0] - i][self.location[1]].is_white() == self.is_black():
                squares.append([self.location[0] - i, self.location[1]])
        except:
            pass

        i = 1
        while (
            self.location[1] + i < 8
            and board[self.location[0]][self.location[1] + i] == 0
        ):
            squares.append([self.location[0], self.location[1] + i])
            i += 1
        try:
            if board[self.location[0]][self.location[1] + i].is_white() == self.is_black():
                squares.append([self.location[0], self.location[1] + i])
        except:
            pass

        i = 1
        while (
            self.location[1] - i >= 0
            and board[self.location[0]][self.location[1] - i] == 0
        ):
            squares.append([self.location[0], self.location[1] - i])
            i += 1
        try:
            if board[self.location[0]][self.location[1] - i].is_white() == self.is_black():
                squares.append([self.location[0], self.location[1] - i])
        except:
            pass

        return squares


class Bishop(Piece):
    def __init__(self, color, location):
        super().__init__(color, location)

    def legal_squares(self):
        squares = []

        i = 1
        while (
            self.location[0] + i < 8
            and self.location[1] + i < 8
            and board[self.location[0] + i][self.location[1] + i] == 0
        ):
            squares.append([self.location[0] + i, self.location[1] + i])
            i += 1
        try:
            if board[self.location[0] + i][self.location[1] + i].is_white() == self.is_black():
                squares.append([self.location[0] + i, self.location[1] + i])
        except:
            pass

        i = 1
        while (
            self.location[0] - i >= 0
            and self.location[1] + i < 8
            and board[self.location[0] - i][self.location[1] + i] == 0
        ):
            squares.append([self.location[0] - i, self.location[1] + i])
            i += 1
        try:
            if board[self.location[0] - i][self.location[1] + i].is_white() == self.is_black():
                squares.append([self.location[0] - i, self.location[1] + i])
        except:
            pass

        i = 1
        while (
            self.location[0] + i < 8
            and self.location[1] - i >= 0
            and board[self.location[0] + i][self.location[1] - i] == 0
        ):
            squares.append([self.location[0] + i, self.location[1] - i])
            i += 1
        try:
            if board[self.location[0] + i][self.location[1] - i].is_white() == self.is_black():
                squares.append([self.location[0] + i, self.location[1] - i])
        except:
            pass

        i = 1
        while (
            self.location[0] - i >= 0
            and self.location[1] - i >= 0
            and board[self.location[0] - i][self.location[1] - i] == 0
        ):
            squares.append([self.location[0] - i, self.location[1] - i])
            i += 1
        try:
            if board[self.location[0] - i][self.location[1] - i].is_white() == self.is_black():
                squares.append([self.location[0] - i, self.location[1] - i])
        except:
            pass

        return squares


class Queen(Rook, Bishop):
    def __init__(self, color, location):
        super().__init__(color, location)

    def legal_squares(self):
        squares = []
        squares.extend(Rook.legal_squares(self))
        squares.extend(Bishop.legal_squares(self))
        return squares


class King(Piece):
    def __init__(self, color, location):
        super().__init__(color, location)

    def legal_squares(self):
        squares = []

        for x in [-1, 0, 1]:
            for y in [-1, 0, 1]:
                if (
                    0 <= self.location[0] + x <= 7
                    and 0 <= self.location[0] + y <= 7
                ):
                    if (
                        board[self.location[0] + x][self.location[1] + y] == 0
                        or board[self.location[0] + x][self.location[1] + y].is_white == self.is_black
                    ):
                        squares.append([self.location[0] + x, self.location[1] + y])

        if (
            self.not_moved
            and isinstance(board[self.location[0] - 3][self.location[1]], Rook)
            and board[self.location[0] - 3][self.location[1]].not_moved
            and board[self.location[0] - 2][self.location[1]] == 0
            and board[self.location[0] - 1][self.location[1]] == 0
        ):
            squares.append([self.location[0] - 2, self.location[1]])

        if (
            self.not_moved
            and isinstance(board[self.location[0] + 4][self.location[1]], Rook)
            and board[self.location[0] + 4][self.location[1]].not_moved
            and board[self.location[0] + 3][self.location[1]] == 0
            and board[self.location[0] + 2][self.location[1]] == 0
            and board[self.location[0] + 1][self.location[1]] == 0
        ):
            squares.append([self.location[0] + 2, self.location[1]])

        return squares

wpa = Pawn('white', [0,1])
wpb = Pawn('white', [1,1])
wpc = Pawn('white', [2,1])
wpd = Pawn('white', [3,1])
wpe = Pawn('white', [4,1])
wpf = Pawn('white', [5,1])
wpg = Pawn('white', [6,1])
wph = Pawn('white', [7,1])
wk = King('white', [3,0])
wq = Queen('white', [4,0])
wra = Rook('white', [0,0])
wrh = Rook('white', [7,0])
wbc = Bishop('white', [2,0])
wbf = Bishop('white', [5,0])
wnb = Knight('white', [1,0])
wng = Knight('white', [6,0])

bpa = Pawn('black', [0,6])
bpb = Pawn('black', [1,6])
bpc = Pawn('black', [2,6])
bpd = Pawn('black', [3,6])
bpe = Pawn('black', [4,6])
bpf = Pawn('black', [5,6])
bpg = Pawn('black', [6,6])
bph = Pawn('black', [7,6])
bk = King('black', [3,7])
bq = Queen('black', [4,7])
bra = Rook('black', [0,7])
brh = Rook('black', [7,7])
bbc = Bishop('black', [2,7])
bbf = Bishop('black', [5,7])
bnb = Knight('black', [1,7])
bng = Knight('black', [6,7])

board = [
    [wra,wpa,0,0,0,0,bpa,bra],
    [wnb,wpb,0,0,0,0,bpb,bnb],
    [wbc,wpc,0,0,0,0,bpc,bbc],
    [wk,wpd,0,0,0,0,bpd,bk],
    [wq,wpe,0,0,0,0,bpe,bq],
    [wbf,wpf,0,0,0,0,bpf,bbf],
    [wng,wpg,0,0,0,0,bpg,bng],
    [wrh,wph,0,0,0,0,bph,brh]
]

def s(x,y):
    if board[x][y] == 0:
        return "  "
    elif isinstance(board[x][y], Queen):
        if board[x][y].is_white():
            return "wq"
        else:
            return "bq"
    elif isinstance(board[x][y], Pawn):
        if board[x][y].is_white():
            return "wp"
        else:
            return "bp"
    elif isinstance(board[x][y], Rook):
        if board[x][y].is_white():
            return "wr"
        else:
            return "br"
    elif isinstance(board[x][y], Bishop):
        if board[x][y].is_white():
            return "wb"
        else:
            return "bb"
    elif isinstance(board[x][y], Knight):
        if board[x][y].is_white():
            return "wn"
        else:
            return "bn"
    elif isinstance(board[x][y], King):
        if board[x][y].is_white():
            return "wk"
        else:
            return "bk"

print("\nWelcome to Two Player Terminal Chess!")

white_turn = True
letters = ['a','b','c','d','e','f','g','h']
numbers = ['1','2','3','4','5','6','7','8']

while True:
    if white_turn:
        turn = "white"
    else:
        turn = "black"

    print("\n8 ["+s(0,7)+"]["+s(1,7)+"]["+s(2,7)+"]["+s(3,7)+"]["+s(4,7)+"]["+s(5,7)+"]["+s(6,7)+"]["+s(7,7)+"]")
    print("7 ["+s(0,6)+"]["+s(1,6)+"]["+s(2,6)+"]["+s(3,6)+"]["+s(4,6)+"]["+s(5,6)+"]["+s(6,6)+"]["+s(7,6)+"]")
    print("6 ["+s(0,5)+"]["+s(1,5)+"]["+s(2,5)+"]["+s(3,5)+"]["+s(4,5)+"]["+s(5,5)+"]["+s(6,5)+"]["+s(7,5)+"]")
    print("5 ["+s(0,4)+"]["+s(1,4)+"]["+s(2,4)+"]["+s(3,4)+"]["+s(4,4)+"]["+s(5,4)+"]["+s(6,4)+"]["+s(7,4)+"]")
    print("4 ["+s(0,3)+"]["+s(1,3)+"]["+s(2,3)+"]["+s(3,3)+"]["+s(4,3)+"]["+s(5,3)+"]["+s(6,3)+"]["+s(7,3)+"]")
    print("3 ["+s(0,2)+"]["+s(1,2)+"]["+s(2,2)+"]["+s(3,2)+"]["+s(4,2)+"]["+s(5,2)+"]["+s(6,2)+"]["+s(7,2)+"]")
    print("2 ["+s(0,1)+"]["+s(1,1)+"]["+s(2,1)+"]["+s(3,1)+"]["+s(4,1)+"]["+s(5,1)+"]["+s(6,1)+"]["+s(7,1)+"]")
    print("1 ["+s(0,0)+"]["+s(1,0)+"]["+s(2,0)+"]["+s(3,0)+"]["+s(4,0)+"]["+s(5,0)+"]["+s(6,0)+"]["+s(7,0)+"]")
    print("   a   b   c   d   e   f   g   h")

    print("\nIt's " + turn + " to move. Enter a legal move in the form \"e2 e4\":")
    move = input()

    if (
        len(move) != 5
        or move[0] not in letters
        or move[1] not in numbers
        or move[2] != " "
        or move[3] not in letters
        or move[4] not in numbers
    ):
        print("\nNot a legal move, try again.")

    else:
        move_pair = move.split(" ")
        start = [ord(move_pair[0][0]) - 97, int(move_pair[0][1]) - 1]
        end = [ord(move_pair[1][0]) - 97, int(move_pair[1][1]) - 1]

        if (
            board[start[0]][start[1]] != 0
            and board[start[0]][start[1]].color == turn
            and end in board[start[0]][start[1]].legal_squares()
        ):
            kingside_castle = ["d1 b1", "d8 b8"]
            queenside_castle = ["d1 f1", "d8 f8"]

            if move_pair in kingside_castle:
                board[end[0]][end[1]] = board[start[0]][start[1]]
                board[end[0]][end[1]].location = end
                board[start[0]][start[1]] = 0
                board[start[0] - 1][start[1]] = board[start[0] - 3][start[1]]
                board[start[0] - 1][start[1]].location = [start[0] - 1, start[1]]
                board[start[0] - 3][start[1]] == 0

            elif move_pair in queenside_castle:
                board[end[0]][end[1]] = board[start[0]][start[1]]
                board[end[0]][end[1]].location = end
                board[start[0]][start[1]] = 0
                board[start[0] + 1][start[1]] = board[start[0] + 4][start[1]]
                board[start[0] + 1][start[1]].location = [start[0] + 1, start[1]]
                board[start[0] + 4][start[1]] == 0

            else:
                board[end[0]][end[1]] = board[start[0]][start[1]]
                board[end[0]][end[1]].location = end
                board[start[0]][start[1]] = 0

                board[end[0]][end[1]].not_moved = False
                white_turn = not white_turn

        else:
            print("\nNot a legal move, try again.")